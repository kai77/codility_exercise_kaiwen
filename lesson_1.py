# 1.(BinaryGap)
def solution(N):
    nums = bin(N).replace('0b','')
    count = 0
    maxNum = 0
    for num in nums:
        if num == '0':
            count += 1 
        else:
            if count > maxNum:
                maxNum = count
            count = 0
    print(maxNum)
    
solution(20)
