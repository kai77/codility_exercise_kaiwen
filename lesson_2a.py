# 2a.(CyclicRotation-right)
def solution(A, K):
    for i in range(K):
        A.insert(0,A.pop())
    print(A)
solution([1,2,3,4,5],2) 

'''
# (CyclicRotation-left)
def solution(A, K):
    for i in range(K):
        A.insert(len(A),A[0])
        A.remove(A[0])
    print (A)
solution([1,2,3,4,5],2)    
'''

