# 2b.(OddOccurrencesInArray)
def solution(A):
    for i in A:
        if A.count(i) == 1:
            print(i)

'''
# Method 2
def solution(A):
    for i in range(len(A)):
        count = 0
        for j in range(len(A)):
            if A[j] == A[i]:
                count += 1
        if count == 1:
            print(A[i])
solution([1,2,1,2,3])

# Method 3
from functools import reduce
def solution(A):
    print(reduce(lambda x,y:x^y, A))
solution([1,2,1,3,2])
'''
