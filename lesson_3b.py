# 3b.(PermMissingElem)0~n+1缺失的数字
def solution(A):
    A.sort()
    i = 1; j = len(A) + 1
    while i <= j:
        m = (i+j)//2
        if A[m-1] == m: 
            i = m + 1
        else:
            j = m - 1
    print(i)
solution([2,3,1,5])

'''
# Method 2
def solution(A):
    A.sort()
    j = 1
    if A[0] != 1:
        print(j)
    for i in range(len(A)):
        if j == A[i]:
            j += 1
        else:
            print(j)
solution([1,2,3,5])
'''