# 3c.(TapeEquilibrium)  (small problem: surplus cycle [0:5]-[5:])
def solution(A):
    diff = 0
    minDiff = abs(A[0] - sum(A[1:]))
    for i in range(len(A)-2):
        diff += abs(sum(A[0:(i+2)]) - sum(A[(i+2):]))
        if diff < minDiff:
            minDiff = diff
            diff = 0
        else:
            continue
    print(minDiff)
solution([3,1,2,4,3])
