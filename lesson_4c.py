# 4c.(MissingInteger)
def solution(A):
    maxNum = max(A)
    if maxNum <= 0 :
        return 1
    else:
        for i in range(1,maxNum+1):
            if i not in A:
                print(i)
                break
            elif i == maxNum:
                print(maxNum+1)
solution([4,2,3,1,6,5])

'''
# Method 2：
def solution(A):
    B = set((i for i in A if i > 0))
    if B:
        print(next((i for i in range(1,len(B) + 2) if not i in B),1))
    else:
        print(1)
solution([2,3,1,6,5])
'''
