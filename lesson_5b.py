# 5b.(GenomicRangeQuery)
def solution(S, P, Q):
    S = list(S)
    minNums = []
    for i, letter in enumerate(S):
        if letter == 'A':
            S[i] = 1
        elif letter == 'C':
            S[i] = 2
        elif letter == 'G':
            S[i] = 3
        elif letter == 'T':
            S[i] = 4
    for i in range(len(P)):
        minNum = min(S[P[i]:Q[i]+1])
        minNums.append(minNum)
    print(minNums)
    
solution('CAGCCTA',[2,5,0],[4,5,6])
