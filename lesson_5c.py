# 5c.(MinAvgTwoSlice)
def solution(A):
    min_index = 0
    min_Avg = (A[0] + A[1]) / 2.0
 
    for i in range(0, len(A)-1):
        if (A[i] + A[i+1])/2.0 < min_Avg:
            min_index = i
            min_Avg = (A[i] + A[i+1])/2.0

        if i < len(A)-2 and (A[i] + A[i+1] + A[i+2])/3.0 < min_Avg:
            min_index = i
            min_value = (A[i] + A[i+1] + A[i+2])/3.0
            
    print(min_index)
    
solution([4,2,2,5,1,5,8])
