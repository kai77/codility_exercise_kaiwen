# 5d.(PassingCars)
def solution(A):
    east = 0
    pairs = 0

    for i in range(len(A)):
        
        if A[i] == 0:
            east += 1
            
        elif A[i] == 1:
            pairs += east
            
            if pairs > 1000000000:
                print(-1)    
                
    print(pairs)
    
solution([0,1,0,1,1])